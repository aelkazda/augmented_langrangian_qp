from typing import Dict, List, Optional, Tuple

import numpy as np
from scipy.linalg import cho_factor, cho_solve, lu_factor, lu_solve, norm

from qp import QuadProgEq

CholeskyFactor = Tuple[np.ndarray, bool]
LuFactor = Tuple[np.ndarray, np.ndarray]


class AugLagEq:
    """Holds the iterate of an equality constrained quadratic program's
    augmented lagrangian.
    """

    def __init__(self, qp: QuadProgEq, x: np.ndarray, p: np.ndarray, u: float):
        """Initialize the iterates

        :x: Primal iterate
        :p: Dual iterate

        """
        assert u > 0
        self.qp: QuadProgEq = qp
        self.x: np.ndarray = x
        self.p: np.ndarray = p
        self.u: float = u

        self._u_old: float = np.nan

        self._hess_factor: Optional[LuFactor] = None
        self._hess_factor_naive: Optional[CholeskyFactor] = None

        self._print_options: Dict = {"history_size": 2 ** 9}

        self._iter: int = -1
        self._obj: List[Optional[float]] = [None] * self._print_options["history_size"]
        self._constr: List[Optional[np.ndarray]] = [None] * self._print_options[
            "history_size"
        ]
        self._update_history()

    def objective_value(self) -> float:
        """Value of the objective function of the quadratic program.

        :returns: 0.5 x.T H x + g.T x

        """
        return 0.5 * self.x.T @ self.qp.H @ self.x + self.qp.g @ self.x

    def constraint_violation(self) -> np.ndarray:
        """Violation of the vector constraint of the quadratic program.

        :returns: A x - b

        """
        return self.qp.A @ self.x - self.qp.b

    def _update_history(self) -> None:
        """Update history of objective values and constraint violation vectors."""
        self._iter = (self._iter + 1) % self._print_options["history_size"]
        self._obj[self._iter] = self.objective_value()
        self._constr[self._iter] = self.constraint_violation()

    def get_history(self) -> Tuple[np.ndarray, np.ndarray]:
        """Returns history of objective values and constraint violation vectors."""
        n = self._print_options["history_size"] - self._obj.count(None)
        obj = np.empty(n)
        constr = np.empty((n, self.p.size))
        for i in range(n):
            obj[i] = self._obj[i + self._iter - n + 1]
            constr[i] = self._constr[i + self._iter - n + 1].copy()
        return obj, constr

    def show(self, diff=False) -> None:
        """Print info about the iterate."""
        i = self._iter
        if not diff:
            print(" {:10}| {}".format("objective", "constraint violation"))
            print(" {:<10.4e}| {:.4e}".format(self._obj[i], norm(self._constr[i])))
        else:
            print(
                " {:21}| {}".format(
                    "objective difference", "constraint violation ratio"
                )
            )
            print(
                " {:<21.4e}| {:.4e}".format(
                    self._obj[i] - self._obj[i - 1],
                    norm(self._constr[i]) / norm(self._constr[i - 1]),
                )
            )

    def step_naive(self) -> None:
        """One iteration of the augmented lagrangian algorithm."""
        qp = self.qp
        if self.u != self._u_old:
            self._hess_factor_naive = cho_factor(qp.H + self.u * qp.A.T @ qp.A)
            self._u_old = self.u

        self.x[:] = -cho_solve(
            self._hess_factor_naive, qp.A.T @ (self.p - self.u * qp.b) + qp.g
        )
        self.p[:] = self.p + self.u * (qp.A @ self.x[:] - qp.b)
        self._update_history()

    def step(self) -> None:
        """One iteration of the (preconditioned) augmented lagrangian algorithm."""
        qp = self.qp
        n = self.x.size
        k = self.p.size
        np.set_printoptions(linewidth=np.inf)

        rhs = np.empty(n + k)
        rhs[:n] = -qp.g
        rhs[n:] = -self.p / self.u + qp.b

        if self.u != self._u_old:
            preconditioned_mat = np.empty((n + k, n + k))
            preconditioned_mat[:n, :n] = qp.H
            preconditioned_mat[:n, n:] = qp.A.T
            preconditioned_mat[n:, :n] = qp.A
            preconditioned_mat[n:, n:] = -np.identity(k) / self.u
            self._hess_factor = lu_factor(preconditioned_mat)
            self._u_old = self.u
        result = lu_solve(self._hess_factor, rhs)

        self.x[:] = result[:n]
        self.p[:] = result[n:]

        self._update_history()
