from __future__ import annotations

import numpy as np
from scipy.stats import ortho_group


def random_hess(n_dim: int, cond: float) -> np.ndarray:
    """Generates a random positive definite symmetric matrix
    with the given condition number.

    :n_dim: Dimension of the vector space.
    :cond: Condition number of the result matrix.
    :returns: Positive definite symmetric matrix of size (n_dim, n_dim).

    """
    assert cond >= 1.0

    ortho_mat = ortho_group.rvs(n_dim)
    log_diag = np.random.rand(n_dim)
    log_diag[0] = 0.0
    log_diag[-1] = 1.0
    log_diag *= np.log(cond)
    diag = np.exp(log_diag)

    return ortho_mat @ (diag * ortho_mat.T)


def random_constraint(n_dim: int, n_constr: int, cond: float) -> np.ndarray:
    """Generates a random linear vector constraint
    with the given condition number.

    :n_dim: Dimension of the vector space.
    :n_constr: Dimension of the constraint vector.
    :cond: Condition number of result matrix.
    :returns:   - A, matrix of size (n_constr, n_dim),
                - b, vector of size n_constr.
    """
    assert cond >= 1.0
    assert n_constr <= n_dim

    ortho_mat_rhs = ortho_group.rvs(n_dim)
    ortho_mat_lhs = ortho_group.rvs(n_constr)

    log_diag = np.random.rand(n_constr)
    log_diag[0] = 0.0
    log_diag[-1] = 1.0
    log_diag *= np.log(cond)
    diag = np.exp(log_diag)

    diag_mat = np.zeros((n_constr, n_dim))
    np.fill_diagonal(diag_mat, diag)
    return ortho_mat_lhs @ diag_mat @ ortho_mat_rhs.T


class QuadProgEq:

    """Holds components of a quadratic program,
    min 0.5 x.T H x + g.T x
    s.t A x = b
    """

    def __init__(
        self,
        obj_hess: np.ndarray,
        obj_grad: np.ndarray,
        constr_jac: np.ndarray,
        constr_rhs: np.ndarray,
    ):
        """Initialize the components' values.

        :obj_hess: Hessian of the objective.
        :obj_grad: Gradient of the objective at the origin.
        :constr_jac: Jacobian matrix of the constraint.
        :constr_rhs: RHS of constraint.

        """
        self.H: np.ndarray = obj_hess
        self.g: np.ndarray = obj_grad
        self.A: np.ndarray = constr_jac
        self.b: np.ndarray = constr_rhs

    @staticmethod
    def rand(
        n_dim: int, n_constr: int, obj_cond: float = 10.0, constr_cond: float = 10.0
    ) -> QuadProgEq:
        """Generates a random convex quadratic program
        with the given condition number.

        :n_dim: Dimension of the vector space
        :cond: Condition number of the result matrix
        :returns: Quadratic program

        """
        return QuadProgEq(
            random_hess(n_dim, obj_cond),
            np.random.randn(n_dim),
            random_constraint(n_dim, n_constr, constr_cond),
            np.random.randn(n_constr),
        )
