import matplotlib.pyplot as plt
import numpy as np

from aug_lag import AugLagEq
from qp import QuadProgEq

if __name__ == "__main__":
    np.random.seed(0)
    n, k = 5, 2
    qp = QuadProgEq.rand(n, k)

    it = AugLagEq(qp, np.zeros(n), np.zeros(k), 1e9)
    it.show()

    for i in range(10):
        it.step()
        print()
        it.show()
    obj_hist, constr_hist = it.get_history()
    obj_min = min(obj_hist)

    fig, ax = plt.subplots(nrows=2, ncols=1)
    data_obj = obj_hist - obj_min
    data_constr = np.linalg.norm(constr_hist, axis=-1)
    ax[0].plot(data_obj)
    ax[0].set_ylim(min(data_obj), max(data_obj))
    ax[1].semilogy(data_constr)
    ax[1].set_ylim(min(data_constr), max(data_obj))
    fig.show()
